# Weather Quality

A simple tool to trend weather and air quality for a given zip code (US Only). 

![alt text](https://gitlab.com/zach.migliozzi/weather-quality/-/wikis/uploads/f4a4a1ef69d4c87365872a1882b2cab4/image.png)

## Requirements
- python 3.8+
- virtualenv
- influxdb 1.8+
- docker
- AirNow api key (free) https://docs.airnowapi.org/account/request/.
- Openweather api key (free) https://openweathermap.org/.

## Get started
- Copy create an .env file from .env.example. `cp .env.example .env`
- Build the application: `docker-compose build`
- Run the application: `docker-compose up -d`
- Login to grafana `admin:admin` http://localhost:3000
- Add the influx data source http://localhost:8086

## Todo
- Create and expose flask web server homepage to display current condtions.
- Nest API 


## License

This project is open-source software licensed under the [MIT license](http://opensource.org/licenses/MIT).

Lastly, shout out to https://github.com/bastienwirtz/aqi_watcher for the inspiration.
