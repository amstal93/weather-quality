import os

INFLUXDB = {
    "URL": "",
    "PORT": "8086",
    "SSL": False,
    "DB": os.environ['INFLUXDB_DB'],
    "USER": os.environ['INFLUXDB_ADMIN_USER'],
    "PASSWORD": os.environ['INFLUXDB_ADMIN_PASSWORD'],
}

POSTCODE = os.environ['POSTCODE']
AIRNOW_KEY = os.environ['AIRNOW_KEY']
OPENWEATHERMAP_KEY = os.environ['OPENWEATHERMAP_KEY']
PURPLEAIR_SENSORS = ""  # coma separated list