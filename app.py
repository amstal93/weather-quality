from influxdb import InfluxDBClient
import requests

import datetime
import os
import sys
import settings
import time
from data_source import openweathermap, airnow

client = None
dbname = settings.INFLUXDB["DB"]

def db_exists():
    '''returns True if the database exists'''
    dbs = client.get_list_database()
    for db in dbs:
        if db['name'] == dbname:
            return True
    return False

def wait_for_server(host, port, nretries=5):
    '''wait for the server to come online for waiting_time, nretries times.'''
    url = 'http://{}:{}'.format(host, port)
    waiting_time = 60
    for i in range(nretries):
        try:
            requests.get(url)
            return 
        except requests.exceptions.ConnectionError:
            print('waiting for', url)
            time.sleep(waiting_time)
            waiting_time *= 2
            pass
    print('cannot connect to', url)
    sys.exit(1)

def connect_db(host, port, reset):
    '''connect to the database, and create it if it does not exist'''
    global client
    print('connecting to database: {}:{}'.format(host,port))
    client = InfluxDBClient(host, port, retries=5, timeout=1)
    wait_for_server(host, port)
    create = False
    if not db_exists():
        create = True
        print('creating database... %s', dbname)
        client.create_database(dbname)
    else:
        print('database already exists')
    client.switch_database(dbname)
    if not create and reset:
        client.delete_series(database=dbname)

def get_weather():
  ''' Attempt to retrieve weather and air quality information.
  '''
  points = []

  # Get weather
  try:
      points += openweathermap.get_points(settings.POSTCODE)
  except Exception as error:
      print("Failed to get weather data: ", error)

  # Get Air Quality Index
  try:
      points += airnow.get_points(settings.POSTCODE)
  except Exception as error:
      print("Failed to get Air Quality data: ", error)

  client.write_points(points,database=dbname)
  
    
if __name__ == '__main__':
    import sys
    
    from optparse import OptionParser
    parser = OptionParser('%prog [OPTIONS] <host> <port>')
    parser.add_option(
        '-r', '--reset', dest='reset',
        help='reset database',
        default=False,
        action='store_true'
        )
    parser.add_option(
        '-n', '--nmeasurements', dest='nmeasurements',
        type='int', 
        help='reset database',
        default=0
        )
    
    options, args = parser.parse_args()
    if len(args)!=2:
        parser.print_usage()
        print('please specify two arguments')
        sys.exit(1)
    host, port = args
    settings.INFLUXDB["URL"] = args[0]
    
    connect_db(host, port, options.reset)

    while True:
      print('Getting weather...')
      get_weather()
      time.sleep(60)